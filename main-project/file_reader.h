#pragma once

#ifndef FILE_READER_H
#define FILE_READER_H

#include "roseWind.h"

void read(const char* file_name, roseWind* array[], int& size);

#endif